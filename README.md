# JFXDemo

Very simple JavaFX demo project

To run the demo use the following command:

```
mvn compile exec:java -Dexec.mainClass=csis10a.jfxdemo.App
```

Once the demo is running you can use the arrow keys to navigate the lady bug around the screen.
