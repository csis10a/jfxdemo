package csis10a.jfxdemo;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;

/**
 * Hello world!
 *
 */
public class App  extends Application {
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle(getClass().getSimpleName());

        // Use Border pane to center content
        Group root = new Group();
        Scene scene = new Scene(root, 800, 600, Color.BLACK);

        // --- Add our circle
        final Circle circle = new Circle(400, 300, 10, Color.RED);
        root.getChildren().add(circle);

        // Add our bug
        Image image  = new Image("bug-icon.png");
        final ImageView iv = new ImageView(image);
        iv.setFitWidth(60);
        iv.setPreserveRatio(true);
        iv.setSmooth(true);
        iv.setCache(true);
        root.getChildren().add(iv);
        EventHandler<KeyEvent> kh2 = new EventHandler<KeyEvent>() {
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.RIGHT) {
                    iv.setX(iv.getX() + 5);
                    iv.setRotate(90);
                }
                else if (keyEvent.getCode() == KeyCode.LEFT) {
                    iv.setX(iv.getX() - 5);
                    iv.setRotate(-90);
                }
                else if (keyEvent.getCode() == KeyCode.UP) {
                    iv.setY(iv.getY() - 5);
                    iv.setRotate(0);
                }
                else if (keyEvent.getCode() == KeyCode.DOWN) {
                    iv.setY(iv.getY() + 5);
                    iv.setRotate(180);
                }
            }
        };
        scene.setOnKeyPressed(kh2);
        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                consume(circle, iv);
            }
        });


        stage.setScene(scene);
        stage.show();
    }

    private void consume(Circle circle, ImageView iv) {
        if (iv.intersects(circle.getBoundsInLocal())) {
            circle.setFill(Color.BEIGE);
        }
    }

}
